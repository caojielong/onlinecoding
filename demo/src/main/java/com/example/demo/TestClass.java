package com.example.demo;

import com.example.demo.jdkDynamicProxy.RealSubject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TestClass {
    public static void main(String[] args) {
        BigDecimal bd = null;
        Map<String,Object> map = new HashMap<>();
        map.put("bd",bd);
        Object dd = (Double)map.get("bd");
        BigDecimal hh = (BigDecimal)dd;
        System.out.println(hh);
    }
}
