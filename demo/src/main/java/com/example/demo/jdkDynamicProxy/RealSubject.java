package com.example.demo.jdkDynamicProxy;

import java.io.Serializable;

/**
 * RealSubject
 * 真实主题类
 * @author
 * @create 2018-03-29 14:21
 **/
public class RealSubject implements Subject, Serializable {
    @Override
    public void doSomething() {
        System.out.println("RealSubject do something");
    }

    @Override
    public void doOtherThings() {
        System.out.println("I am doing other things!");
    }
}
