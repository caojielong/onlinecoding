package com.example.demo.mongo;

import lombok.*;
import org.bson.Document;

@Data
public class Student {
    String name;
    int age;
    int grade;
    String sex;

    public Student() {
    }

    public Student(String name, int age, int grade, String sex) {
        this.name = name;
        this.age = age;
        this.grade = grade;
        this.sex = sex;
    }
}
