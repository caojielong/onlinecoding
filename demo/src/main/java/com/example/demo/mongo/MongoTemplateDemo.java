package com.example.demo.mongo;

import com.alibaba.fastjson.JSON;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MongoTemplateDemo {
    public static void main(String[] args) {
        int testCase = 1;

        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        MongoTemplate mongoTemplate = (MongoTemplate)ac.getBean("mongoTemplate");


        //student.insertOne(new Document("name","Jim").append("age",22));
        /*Student s1 = new Student("Ming",12,78,"女");
        Student s2 = new Student();
        s2.setName("Tome");
        List<Student> ss = new ArrayList<>();
        ss.add(s1);
        ss.add(s2);
        mongoTemplate.insert(ss,"student");*/

        switch (testCase){
            case 1://查找相同的名字，以及相同的行数
                Criteria c = Criteria.where("总人数").gt(1);

                Aggregation nameAgg = Aggregation.newAggregation(Aggregation.group("name").count().as("总人数"),
                        Aggregation.match(c));
                AggregationResults<BasicDBObject> nameAggRes =
                        mongoTemplate.aggregate(nameAgg, "student", BasicDBObject.class);

                //Aggregation agg = Aggregation.newAggregation(Aggregation.group("age"),Aggregation.limit(2));
                List<BasicDBObject> mappedResults = nameAggRes.getMappedResults();
                Iterator<BasicDBObject> iterator = nameAggRes.iterator();
                System.out.println("====================打印结果=======================");
                while(iterator.hasNext()) {
                    BasicDBObject obj = iterator.next();
                    System.out.println(obj);
                }
                break;
            case 2://查询
                //MongoCollection<Document> student = mongoTemplate.getCollection("student");
                DBCollection student = mongoTemplate.getCollection("student");
                DBCursor dbObjects = student.find();
                System.out.println("====================打印结果=======================");
                while(dbObjects.hasNext()){
                    System.out.println(dbObjects.next());
                }
                break;
        }

        /*FindIterable<Document> documents = student.find();
        MongoCursor<Document> iterator = documents.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }*/
    }
}
