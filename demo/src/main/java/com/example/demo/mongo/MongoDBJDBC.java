package com.example.demo.mongo;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

public class MongoDBJDBC{
    public static void main( String args[] ){
        try{
            // 连接到 mongodb 服务
            MongoClient mongoClient = new MongoClient( "192.168.42.140" , 27017 );

            // 连接到数据库
            MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
            System.out.println("Connect to database successfully");

            MongoCollection<Document> collection = mongoDatabase.getCollection("student");

            System.out.println("集合 test 选择成功");

            //检索所有文档
            /**
             * 1. 获取迭代器FindIterable<Document>
             * 2. 获取游标MongoCursor<Document>
             * 3. 通过游标遍历检索出的文档集合
             * */

            //collection.find(new BSON("name","Jay"))
            Bson filter = Filters.eq("name", "Tim");
            FindIterable<Document> findIterable = collection.find(filter);
            MongoCursor<Document> mongoCursor = findIterable.iterator();
            System.out.println("====================打印结果=======================");
            while(mongoCursor.hasNext()){
                System.out.println(mongoCursor.next());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
