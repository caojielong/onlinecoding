package com.example.demo.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.ClassPathXmlApplicationContext;
@ComponentScan("com.example.demo.aop")
@EnableAspectJAutoProxy
public class Test {

    public static void main(String[] args) {

        //ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
        AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext(Test.class);
        Person bean2 = (Person) ac.getBean("student");
        bean2.say();
    }
}