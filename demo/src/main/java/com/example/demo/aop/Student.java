package com.example.demo.aop;

import org.springframework.stereotype.Service;

@Service("student")
public class Student implements Person{
    @Override
    public void say() {
        System.out.println("一个苦逼的程序员");
    }
}
