import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        Task solver = new Task();
        solver.solve(1, in, out);
        out.close();
    }

    static class Task {
        int[] a;
        int[] b;
        int n;
        Map<String,Long> cache=new HashMap<>();
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            n = in.nextInt();
            a = new int[n];
            b = new int[n];
            for(int i = 0 ; i < n ; i++){
                a[i] = in.nextInt();
            }
            for(int i = 0 ; i < n ; i++){
                b[i] = in.nextInt();
            }
            Node a0 = new Node(0,a[0],true);
            Node b0 = new Node(0,b[0],false);
            out.println(Math.max(countMax(a0),countMax(b0)));
        }

        private long countMax(Node topNode) {
            String key = (topNode.type?"A":"B")+topNode.index;
            long value;
            if(cache.containsKey(key)){
                return cache.get(key);
            }else if(topNode.index==n-1){
                value = topNode.value;
            }else if(topNode.index==n-2){
                if(topNode.type){
                    value = topNode.value + b[n-1];
                }else{
                    value = topNode.value + a[n-1];
                }
            }else{
                if(topNode.type){
                    value = topNode.value +
                            Math.max(countMax(new Node(topNode.index+1,b[topNode.index+1],false)),
                                    countMax(new Node(topNode.index+2,b[topNode.index+2],false)));

                }else{
                    value = topNode.value +
                            Math.max(countMax(new Node(topNode.index+1,a[topNode.index+1],true)),
                                    countMax(new Node(topNode.index+2,a[topNode.index+2],true)));

                }
            }
            cache.put(key,value);
            return value;
        }

        static class Node{
            int index;
            int value;
            boolean type;

            public Node(int index, int value,boolean type) {
                this.index = index;
                this.value = value;
                this.type = type;
            }
        }
    }

    static class InputReader {
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public Long nextLong() {
            return Long.parseLong(next());
        }

        public Double nextDouble() {
            return Double.parseDouble(next());
        }
    }

    public static int[] inputArr(int n, InputReader in) {
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        return a;
    }

    public static int[] inputArrFrom1(int n, InputReader in) {
        int[] a = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            a[i] = in.nextInt();
        }
        return a;
    }

    public static void printArr(int[] a, PrintWriter out) {
        if (a.length > 0) {
            for (int i = 0; i < a.length - 1; ++i) {
                out.print(a[i] + " ");
            }
            out.println(a[a.length - 1]);
        }
    }

    public static long gcd(long a, long b) {
        while (b > 0) {
            long c = a;
            a = b;
            b = c % b;
        }
        return a;
    }

    // C(n,m)=C(n,n-m）。（n≥m)
    // Cn0+Cn1+...+Cnn = 2^n
}
